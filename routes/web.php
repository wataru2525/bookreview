<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->intended('/login');
});
Route::get('/books', 'BookController@index');

Route::get('about', 'HelpController@about')->name('about');

Route::post('books/{id}/review', 'ReviewController@createReview')->name('create.review');

Route::resource('books', 'BookController');
Route::resource('categories', 'CategoryController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
