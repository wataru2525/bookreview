@extends('main')
@section('title', '| 投稿一覧')
@section('content')
  <a href="{{ route('books.create') }}" class="btn btn-warning">新しい投稿</a>
  <div style="padding:20px 0 20px 0;">
    <form class="form-inline" action="{{url('/books')}}">
      <div class="form-group">
        <input type="text" name="keyword" value="{{$keyword}}" class="form-control" placeholder="タイトル検索">
      </div>
      <input type="submit" value="検索" class="btn btn-info">
    </form>
  </div>
</div>
  @if($books->count())
  @foreach($books->chunk(3) as $items)
    <div class="row">
      @foreach ($items as $book)
        <div class="col-md-4">
            <div class="well"　>
                <img src="{{asset($book->cover ? '/images/' . $book->cover : '/images/' . "default.png")}}" class="text-center" width="300px" height="400px"　/>
                <h3><a href="{{ route('books.show', $book->id) }}">{{ $book->title }}</a></h3>
            </div>
        </div>
      @endforeach
    </div>
  @endforeach
  @else
  <h3 class="text-center">該当する本が見つかりません。</h3>
  @endif
@endsection