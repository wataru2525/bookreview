<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
  public function user()
  {
      return $this->belongsTo('App\User','foreign_key');
  }

  public function book()
  {
      return $this->belongsTo('App\Book', 'foreign_key');
  }
}
