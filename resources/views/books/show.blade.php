@extends('main')
@section('title', '| レビュー投稿ページ')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="text-center well">
        <img
        src="{{asset($books->cover ? '/images/' . $books->cover : '/images/' . "default.png")}}" style="margin-left: 35%" class="img-responsive" width="300px" height="300px" />
        <h1>{{ $books->title }}</h1>
        <p class="lead">{{ $books->description }}</p>
    </div>
    <div class="row" style="margin-top:40px;">
      <div class="col-md-12">
        <div class="well well-sm">
          <h4 class="text-center">
            レビュー記入欄
          </h4>
          <div class="row" id="post-review-box">
            <div class="col-md-12">
              <form action="{{ route('create.review', $books->id) }}" accept-charset="UTF-8" action="" method="post">
              {{Form::token()}}
              <input type="hidden" name="book_id"  value="{{ $books->id }}">
                <textarea required class="form-control animated" cols="50" id="new-review" name="comment" placeholder="レビューを記入" rows="5"></textarea>
                <div class="text-right">
                  <button class="btn btn-info btn-lg" type="submit">投稿</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    @foreach($books->reviews as $review)
    <hr>
    <div class="row">
      <div class="col-md-12">
        <h4>{{ $review->comment }}</h4>
          @if($review->created_at)
            <p>{{ $review->created_at->diffForHumans() }} time ago</p>
          @endif
      </div>
    </div>
  @endforeach
</div>
@endsection
@section('scripts')
<script src="{{asset('js/star-custom.js')}}"></script>
@endsection