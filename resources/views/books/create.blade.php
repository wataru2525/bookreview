@extends('main')
@section('title', '| 新しい本を追加')
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <h1 class="text-center">本の登録</h1>
    <hr>
    {!! Form::open(array('route' => 'books.store', 'method' => 'POST', 'files' => true)) !!}
    {{Form::token()}}

      {{ Form::label('title', 'タイトル:') }}
      {{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

      {{ Form::label('author', '著者:') }}
      {{ Form::text('author', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

      {{ Form::label('category_id', 'ジャンル:') }}
			<select class="form-control" name="category_id">
      <option value='0'>ジャンル選択</option>
        @foreach($categories as $category)
          <option value='{{ $category->id }}'>{{ $category->name }}</option>
        @endforeach
      </select>
      
      {{ Form::label('cover', '画像') }}
      {{ Form::file('cover') }}

      {{ Form::label('description', "概要:") }}
      {{ Form::textarea('description', null, array('class' => 'form-control')) }}

      {{ Form::submit('投稿', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
    {!! Form::close() !!}
  </div>
</div>
@endsection