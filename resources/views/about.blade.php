@extends('main')
@section('title', '| Webサイトについて')
@section('content')
<h2 class="text-center">ようこそブックレビューアプリへ</h2>
<h4 class="text-center">好きな本を共有して感想を投稿しよう！！</h4>
<div class="row">
  <div class="col-md-4">
      <img src="{{ asset('/images/about.jpg') }}" width="300px" height="400px">
  </div>
  <div class="col-md-4">
      <img src="{{ asset('/images/bible.jpg') }}" width="300px" height="400px">
  </div>
  <div class="col-md-4">
      <img src="{{ asset('/images/sea.jpg') }}" width="300px" height="400px">
  </div>
</div>
@endsection