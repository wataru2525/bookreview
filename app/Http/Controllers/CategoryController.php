<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

use Validator;


class CategoryController extends Controller
{
  public function create()
    {
        return view('categories.create');
    }

  public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          'name' => 'required|max:50|min:2',
        ]);
        if ($validator->fails()) {
          return back()->withInput()->withErrors($validator);
        }
        $book = new Category();
        $book->name = $request->name;
        $book->save();
        return redirect()->intended('books');
    }
}
