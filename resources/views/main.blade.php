<!DOCTYPE html>
<html lang="ja">
  <head>
      @include('partials._shim')
  </head>
  <body>
      @include('partials._navbar')
      <div class="container">
          @include('partials._messages')
          @yield('content')
      </div>
      <hr>
      @include('partials._javascripts')
      @yield('scripts')
  </body>
</html>