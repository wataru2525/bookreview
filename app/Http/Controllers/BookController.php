<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use Image;
use Validator;

class BookController extends Controller
{
  public function index(Request $request)
    {
        $keyword = $request->input('keyword');
        $query = Book::query();
        if (!empty($keyword)) {
            $query->where('title', 'LIKE', "%{$keyword}%");
        }
        $books = $query->get();
        return view('books.index', compact('books', 'keyword'));
        
    }

    public function create()
    {
        $categories = Category::all();
        return view('books.create')->withCategories($categories);
    }

    public function store(Request $request)
      {
          $validator = Validator::make($request->all(), [
            'title' => 'required|max:255|min:２２',
            'description' => 'required',
            'category_id' => 'required',
            'author' => 'required|max:255'
          ]);
          if ($validator->fails()) {
              return back()->withInput()->withErrors($validator);
          }

          $book = new Book();
          $book->title = $request->title;
          $book->description = $request->description;
          $book->author = $request->author;
          $book->category_id = $request->category_id;

          if ($request->hasFile('cover')) {
              $image = $request->file('cover');
              $filename = $request->file('cover')->getClientOriginalName();
              $location = public_path('images/' . $filename);
              if(!file_exists($location))
              {
                Image::make($image)->resize(300, 400)->save($location);
              }
              $book->cover = $filename;
          }
          $book->save();
          return redirect()->route('books.index');
    }

    public function show($id)
      {
          $book = Book::findOrFail($id);
          return view('books/show', [
              'books' => $book,
          ]);
      }
}
