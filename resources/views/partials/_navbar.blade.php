<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
      <a class="navbar-brand" href="#">ブックレビューサイト</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="{{Request::is('about') ? "active":""}}"><a href="/about">サイトについて</a></li>
        <li class="{{Request::is('books') ? "active":""}}"><a href="/books">レビュー</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ジャンル<span class="caret"></span></a>
          <ul class="dropdown-menu">
              @foreach (App\Category::all() as $cat)
                  <li><a href="#">{{ $cat->name }}</a></li>
              @endforeach
              <li role="separator" class="divider"></li>
              <li><a href="{{ route('categories.create') }}">新しいカテゴリ</a></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        @if(Auth::check())
        <li>
          <form name="F" method="post" action="/logout" class="logout_form">
            @csrf
            <input type=hidden name=logout_form >
            <a href="javascript:F.submit()">ログアウト</a>
          </form>
        </li>
          <li><a href="#">{{ Auth::user()->name }}</a></li>
        @else
          <li><a href="{{ route('login') }}">ログイン</a></li>
          <li><a href="{{ route('register') }}">新規登録</a></li>
        @endif
      </ul>
    </div>
  </div>
</nav>